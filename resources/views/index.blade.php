@extends('layouts.app')

@section('content')
  @include('partials.page-header')
  @include('partials.chunk-index')
  {!! get_the_posts_navigation() !!}
@endsection
