<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap container" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
    <!-- Simplified URL AdSlot 1 for Ad Unit 'Publirreportajes/2019/Julio/ChileFreelance_landing' ### Size: 1x1 -->
    <a href='https://pubads.g.doubleclick.net/gampad/jump?iu=/124506296/Publirreportajes/2019/Julio/ChileFreelance_landing&sz=1x1&c=[TIMESTAMP]'>
      <img src='https://pubads.g.doubleclick.net/gampad/ad?iu=/124506296/Publirreportajes/2019/Julio/ChileFreelance_landing&sz=1x1&c=[TIMESTAMP]'/>
    </a>

    <!-- End -->
    </body>
</html>
