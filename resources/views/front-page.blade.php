@extends('layouts.app')

@section('content')
  @include('partials.chunk-main')
  @include('partials.chunk-categories')
@endsection