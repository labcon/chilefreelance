@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-single-'.get_post_type())
    @php
      $cat = get_the_category($post->ID);
    @endphp
    <div class="wrap-category-chunk">
      <h2>
        Más de {{($cat[0]->cat_name)}}:
      </h2>
    </div>
    @include('partials.chunk-by-category', array('cat' => $cat[0], 'counter' => 0, 'hasTitle' => false))
    @include('partials.chunk-main')
  @endwhile
@endsection
