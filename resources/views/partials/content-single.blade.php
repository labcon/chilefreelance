@php
  $cat = get_the_category($post->ID);
@endphp
<article @php post_class() @endphp>
  @if(get_post_format() === 'video')
    <header class="single-header">
      <div class="video-wrap">
        <div class="iframe-wrap">
        {!! get_field('iframe') !!}
        </div>
      </div>
      <h1 class="entry-title">
        @if(get_field('pretitulo', $post->ID))
          <span class="entry-prevtitle">
            {{get_field('pretitulo', $post->ID)}}
          </span>
        @endif
        {!! get_the_title() !!}
        @if(get_field('posttitulo', $post->ID))
          <span class="entry-posttitle">
            {{get_field('posttitle', $post->ID)}}
          </span>
        @endif
      </h1>
      @include('partials.chunk-meta-single', array('post' => $post))
    </header>
  @else
    <header class="single-header">
      <h1 class="entry-title">
        @if(get_field('pretitulo', $post->ID))
          <span class="entry-prevtitle">
            {{get_field('pretitulo', $post->ID)}}
          </span>
        @else
          <span class="entry-prevtitle">
            {{($cat[0]->cat_name)}}
          </span>
        @endif
        {!! get_the_title() !!}
        @if(get_field('posttitulo', $post->ID))
          <span class="entry-posttitle">
            {{get_field('posttitle', $post->ID)}}
          </span>
        @endif
      </h1>
      @include('partials.chunk-meta-single', array('post' => $post))
      <div class="figure-wrap">
        <figure>
            @php the_post_thumbnail('lt-single') @endphp
        </figure>
      </div>

    </header>
  @endif

  <div class="entry-content">
    @if (!empty($post->post_excerpt))
      <p class="entry-excerpt">
      {!! get_the_excerpt() !!}
      </p>
    @endif

    @php the_content() @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php
  // comments_template('/partials/comments.blade.php')
  @endphp
</article>
