<time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
<div class="sharer">
  <a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(get_permalink($post->ID))}}">
    <i class="fab fa-facebook-f"></i>
  </a>
<a href="https://twitter.com/share?text={{urlencode($post->post_title)}}&url={{urlencode(get_permalink($post->ID))}}">
    <i class="fab fa-twitter"></i>
  </a>
  <a href="https://wa.me/?text={{urlencode($post->post_title.' '.get_permalink($post->ID))}}">
    <i class="fab fa-whatsapp"></i>
  </a>
</div>
{{-- <p class="byline author vcard">
  {{ __('By', 'sage') }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn">
    {{ get_the_author() }}
  </a>
</p> --}}
