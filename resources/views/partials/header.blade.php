<header class="site-header">
  <div class="container">
    <nav class="nav-primary">
      <a class="brand" href="{{ home_url('/') }}">
        <img src="@asset('images/logo-chilefreelance.svg')">
      </a>
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
    </nav>
  </div>
</header>
