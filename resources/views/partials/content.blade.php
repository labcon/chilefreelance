<article @php post_class('article-loop') @endphp>
  <header>
    <h2 class="entry-title">
      <a href="{{ get_permalink() }}">
        <figure>
        @php the_post_thumbnail('lt-thumb') @endphp
        </figure>
        <div class="entry-description">
          @if(get_field('pretitulo'))
            <span class="entry-prevtitle">
              {{get_field('pretitulo')}}
            </span>
          @endif
          <span class="entry-description-title">{{the_title()}}</span>
          @if(get_field('posttitulo'))
            <span class="entry-posttitle">
              {{get_field('posttitle')}}
            </span>
          @endif
        </div>
      </a>
    </h2>
  </header>
  <div class="entry-summary">
    @php
      // the_excerpt()
    @endphp
  </div>
</article>
