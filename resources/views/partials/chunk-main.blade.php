@php
  if(empty($numberposts)) $numberposts = 5
@endphp
<section class="chunk chunk-main">
  @php
  $queryarr = array(
    'numberposts'	=> $numberposts,
    'post_type'		=> 'post',
    'meta_key'    => 'destacado',
    'meta_value'  => '1'
  );
  @endphp
  @include('partials.component-storyfeed', array('query' => $queryarr, 'hasLink' => true, 'indestacado' => true))
</section>