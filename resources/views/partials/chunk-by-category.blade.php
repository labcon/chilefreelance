@php
  if(empty($numberposts)) $numberposts = 5;
  if(!isset($hasTitle)) $hasTitle = true;
@endphp
<div class="wrap-category-chunk">
  @if($hasTitle)
  <h2>
    <a href="{{get_category_link($cat)}}" class="link-gray">
      {!! $cat->name !!}
    </a>
  </h2>
  @endif
  <div class="chunk chunk-category">
    @php
      $query = '&numberposts='.$numberposts.'&category='.$cat->term_taxonomy_id
    @endphp
    @include('partials.component-storyfeed', array('query' => $query, 'hasLink' => true))
  </div>
</div>