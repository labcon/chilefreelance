@php
  $intquery = get_posts($query);
  if(empty($indestacado)) $indestacado = false
@endphp
@if(!empty($intquery))
  @foreach($intquery as $post)
    @include('partials.component-storycard', array('post' => $post, 'hasLink' => $hasLink, 'indestacado' => $indestacado))
  @endforeach
@endif