<section class="hero">
  <div class="container">
    <a class="brand" href="{{ home_url('/') }}">
      <img src="@asset('images/logo-chilefreelance.svg')">
    </a>
    {!! the_content() !!}
    <span class="bg"></span>
  </div>
</section>