@php
if(empty($hasLink)){
  $hasLink = true; //tiene link por defecto
}
if(!empty($global) && $global === true){
  global $post;
}
if(empty($indestacado)){
  $indestacado = false;
}

$postclass = get_post_class('', $post->ID);
$excerpt = apply_filters( 'get_the_excerpt', $post->post_excerpt, $post );
$ble = get_the_category($post->ID);

$videoenhome = get_field('video_en_home', $post->ID);
$iframevideo = get_field('iframe', $post->ID);

@endphp
<article class="{{ join(' ', $postclass )}}">
  @if($hasLink)
  <a class="wrap" href={{ get_permalink($post->ID) }}>
  @else
  <div class="wrap">
  @endif
    <figure>
    	@if ($videoenhome && $iframevideo && $indestacado)
    	<div class="video-wrap">
        <div class="iframe-wrap">
        {!! get_field('iframe', $post->ID) !!}
        </div>
      </div>
      @else
      {!! get_the_post_thumbnail($post->ID, 'lt-block') !!}
      @endif
    </figure>
    <div class="entry-meta">
      <div class="entry-categories">
        {{$ble[0]->cat_name}}
      </div>
      @if(get_field('pretitulo', $post->ID))
        <span class="entry-prevtitle">
          {{get_field('pretitulo', $post->ID)}}
        </span>
      @endif
      <h1 class="title">{{$post->post_title}}</h1>
      @if(get_field('posttitulo', $post->ID))
        <span class="entry-posttitle">
          {{get_field('posttitle', $post->ID)}}
        </span>
      @endif
      <div class="entry-description">{{$excerpt}}</div>

      @include('partials.chunk-meta', array('post' => $post))

    </div>
  @if($hasLink)
  </a>
  @else
  </div>
  @endif
</article>