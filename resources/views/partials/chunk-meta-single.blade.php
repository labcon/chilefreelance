@php
if (empty($post)) {
  return;
}
$posted = $post->post_date;
$lastmodified = $post->post_modified;
$weekAgo = strtotime('-1 week');

@endphp
<div class="entry-meta-details">
  <span class="entry-meta-author">
    <span class="entry-by">Por:</span> <strong>@php(the_author_meta('display_name', $post->post_author))</strong>
  </span>
  @if(intval(strtotime($post->post_modified)) < intval($weekAgo))
  <span class="entry-meta-time">
    {{ get_post_modified_time('l j F o', false, $post->ID, true) }}
  </span>
  @else
  <span class="entry-meta-ago">
    {{ esc_html('hace '. human_time_diff( get_post_modified_time('U', false, $post->ID) ) ) }}
  </span>
  @endif
  @if(is_amp_endpoint())
    <div class="amp-sharer sharer">
      <amp-social-share type="facebook" data-param-app_id="439827249902197"></amp-social-share>
      <amp-social-share type="twitter"></amp-social-share>
      <amp-social-share type="whatsapp"></amp-social-share>
    </div>
  @else
    <div class="sharer">
      <a href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(get_permalink($post->ID))}}">
        <i class="fab fa-facebook-f"></i>
      </a>
    <a href="https://twitter.com/share?text={{urlencode($post->post_title)}}&url={{urlencode(get_permalink($post->ID))}}">
        <i class="fab fa-twitter"></i>
      </a>
      <a href="https://wa.me/?text={{urlencode($post->post_title.' '.get_permalink($post->ID))}}">
        <i class="fab fa-whatsapp"></i>
      </a>
    </div>
  @endif
</div>