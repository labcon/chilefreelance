<footer class="content-info">
  <div class="container">
    <h3>Sitio presentado por:</h3>
      <a href="https://www.aafp.cl/" target="_blank" rel="noopener noreferrer">
        <img src="@asset('images/logo-asociacion-afp.svg')">
      </a>
      <a href="https://www.laboratoriodecontenidos.cl/" target="_blank" rel="noopener noreferrer">
        <img class="logo-lab" src="@asset('images/logo-laboratorio.svg')">
      </a>
        @php echo chartbeat() @endphp
  </div>
</footer>
