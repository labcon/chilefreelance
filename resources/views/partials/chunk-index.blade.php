@if (!have_posts())
  <div class="alert alert-warning">
    {{ __('Sorry, no results were found.', 'sage') }}
  </div>
  {!! get_search_form(false) !!}
@endif
<div class="wrap-index-chunk">
  <div class="chunk chunk-index">
    @while (have_posts())
      @php the_post() @endphp
      @include('partials.component-storycard', array('hasLink' => true, 'global' => true))
    @endwhile
  </div>
</div>