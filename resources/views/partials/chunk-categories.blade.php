<section class="wrap-chunk">
  @php
    $cats = get_terms('category', array(
      'orderby' => 'term_order'
      )
    );
  @endphp
  @if(!empty($cats))
    @php
      $counter = 0;
    @endphp
    @foreach($cats as $cat)
      @php
        $counter++;
      @endphp
      @include('partials.chunk-by-category', array('cat' => $cat, 'counter' => $counter))
    @endforeach
  @endif
</section>