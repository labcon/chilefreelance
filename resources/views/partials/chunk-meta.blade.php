@php
if (empty($post)) {
  return;
}
$posted = $post->post_date;
$lastmodified = $post->post_modified;
$weekAgo = strtotime('-1 week');

@endphp
<div class="entry-meta-details">
  <span class="entry-meta-author">
    <span class="entry-by">Por:</span> <strong>@php(the_author_meta('display_name', $post->post_author))</strong>
  </span>
  @if(intval(strtotime($post->post_modified)) < intval($weekAgo))
  <span class="entry-meta-time">
    {{ get_post_modified_time('l j F o', false, $post->ID, true) }}
  </span>
  @else
  <span class="entry-meta-ago">
    {{ esc_html('hace '. human_time_diff( get_post_modified_time('U', false, $post->ID) ) ) }}
  </span>
  @endif

</div>