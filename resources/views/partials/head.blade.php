<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700|Open+Sans:400,700|Vollkorn:400,400i&display=swap" rel="stylesheet">

  @php wp_head() @endphp
  @if(!is_amp_endpoint())
  @php echo chartbeat_head() @endphp
  @endif
</head>
