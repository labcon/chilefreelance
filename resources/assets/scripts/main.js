// import external dependencies
import 'jquery';
import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { faFacebookF, faTwitter, faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { faPlay, faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import 'owl.carousel'

// Import everything from autoload
import './autoload/**/*'

library.add(faFacebookF, faTwitter, faWhatsapp, faPlay, faChevronLeft, faChevronRight);

dom.watch();

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
